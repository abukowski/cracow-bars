<?php

return [
    // cracow main square
    'place' => [
        'lat' => 50.061690,
        'lng' => 19.937384,
    ],
    'radius' => 2000
];
