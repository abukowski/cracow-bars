# Bars around the Cracow Main Square

This application shows a map with bars within 2km radius around the Cracow Main Square.

Clicking on the place marker will provide place details in the side panel
and place name in the marker popup. Also the list of available images will
be loaded and shown as simple links. Each photo will open in the new window/tab.

Search bar allows to filter result according to entered keyword.
Search is performed automatically right after user stops writing. To reset
search query just remove the input.

## Technical details

Application is built using [laravel](https://laravel.com/).
It provides a simple GUI component written in [vuejs](https://vuejs.org/)
to handle [leaflet](http://leafletjs.com/) map and api calls to the backend.
Backend is a simple REST API which retrieves places from google according
to the current map center point and map viewport. If places are not within
specified distance from the main place (configurable) they will not be
returned by the API.

Place details and image contents are cached on the server side, but also some
responses report to the browser that they can be cached.

Google Places API key is stored solely on the server and not provided to the user.

Database is not used within the application, also there is no API authentication.

## Installation

1. Clone this repository
2. Run `composer install` to install PHP libraries
3. Copy `.env.example` as `.env`. Adjust `GOOGLE_PLACES_API_KEY`
4. Run `php artisan key:generate`
4. Run `php artisan serve` to run local development server
   or set www root to `/public` directory

## Development

1. Run `yarn` to install JS libraries
2. Run `npm run production` or `npm run dev` to compile resources
3. Use `php artisan` to manage laravel. Take care of
    1. `php artisan config:*` to clear cached config
    2. `php artisan cache:*` to clear application cache if necessary
    3. `php artisan optimize`
4. Run `phpunit` to run tests

## Configuration

Main Square place location and radius can be configured in `config/core.php`
