<?php
/**
 * Created by PhpStorm.
 * User: ajb
 * Date: 05.02.17
 * Time: 17:43
 */

namespace App\Exceptions;

/**
 * Exception thrown when Google Places API gives an error
 *
 * @package App\Exceptions
 */
class GoogleApiException extends JsonResponseException
{

}