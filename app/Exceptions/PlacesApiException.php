<?php
/**
 * Created by PhpStorm.
 * User: ajb
 * Date: 05.02.17
 * Time: 23:19
 */

namespace App\Exceptions;

/**
 * Exception thrown by Places Api controller to return nice response by ErrorHandler
 *
 * @package App\Exceptions
 */
class PlacesApiException extends JsonResponseException
{

}