<?php
/**
 * Created by PhpStorm.
 * User: ajb
 * Date: 05.02.17
 * Time: 23:24
 */

namespace App\Exceptions;

/**
 * Exception class to inherit for API exceptions
 *
 * @package App\Exceptions
 */
class JsonResponseException extends \Exception
{

}