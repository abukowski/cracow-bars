<?php
/**
 * Created by PhpStorm.
 * User: ajb
 * Date: 01.02.17
 * Time: 09:59
 */

namespace App;

use App\Exceptions\GoogleApiException;
use Ballen\Distical\Calculator;
use Ballen\Distical\Entities\LatLong;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Psr\Http\Message\ResponseInterface;

/**
 * Places retrieve from google places service
 *
 * @package App
 */
class Places
{
    /**
     * @var \GuzzleHttp\Client $client
     */
    protected $client;

    /**
     * @var string $placesApiKey
     */
    protected $placesApiKey;

    /**
     * @var string
     */
    protected $googleMapsApiUri;

    /**
     * Places constructor.
     *
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->placesApiKey = config('google.places.key');
        $this->googleMapsApiUri = 'https://maps.googleapis.com/maps/api/place/';
    }

    /**
     * Retrieve bars only 2000m from the Cracow main square location by radar search
     *
     * If location and radius are too far from the main square search
     * will not be performed
     *
     * @param LatLong $location Point to search for places nearby
     * @param int $radius Radius around the location to search in
     * @param array $other Other get params to pass
     * @return array Google places API response
     */
    public function getBarsAroundMainSquare(LatLong $location, $radius, $other = [])
    {
        $maxDistance = 2000;
        if ($this->calculateMainSquareDistance($location) < $radius + $maxDistance) {
            $params = array_merge(['type' => 'bar'], $other);
            $response = $this->getPlaces($location, $radius, $params);
            $response['results'] = $this->filterPlacesFarFromMainSquare($response['results']);
            return $response;
        }
        return ['results' => []];
    }

    /**
     * Google places API search using radar search
     *
     * @param LatLong $location Point to search for places nearby
     * @param int $radius
     * @param array $other Other query params to radarsearch
     * @return array Google places API response
     */
    public function getPlaces(LatLong $location, $radius, $other = [])
    {
        $locationStr = implode(',', [$location->lat(), $location->lng()]);
        $query = array_merge(
            ['location' => $locationStr, 'radius' => $radius],
            $other
        );
        $response = $this->callGoogleMapsApi('radarsearch', $query);
        return $response;
    }

    /**
     * Retrieve place details
     *
     * @param string $placeId Unique google place id
     * @return array
     * @throws GoogleApiException
     */
    public function getPlaceDetails($placeId)
    {
        try {
            $response = $this->callGoogleMapsApi('details', ['placeid' => $placeId]);
        } catch (GoogleApiException $exception) {
            if ($exception->getCode() == 400) {
                throw new GoogleApiException('Place not found', '404');
            } else {
                throw $exception;
            }
        }
        return $response;
    }

    /**
     * Retrieve cached place details
     *
     * @param string $placeId Unique google place id
     * @return array
     */
    public function getCachedPlaceDetails($placeId)
    {
        $cacheKey = 'place_details_' . $placeId;
        $details = Cache::remember($cacheKey, 5, function() use($cacheKey, $placeId) {
            return $this->getPlaceDetails($placeId);
        });
        return $details;
    }

    /**
     * Get Photo response from google
     *
     * @param $photoId
     * @return ResponseInterface
     */
    public function getPhoto($photoId)
    {
        $response = $this->getGoogleMapsApiResponse('', [
            'photoreference' => $photoId,
            'maxwidth' => 600,
            'maxheight' => 600
        ], 'GET', 'photo');
        return $response;
    }

    /**
     * Get response directly from google places API
     *
     * @param string $service Service identifier in uri. e.g. nearbysearch, details, etc
     * @param array $query Query params
     * @param string $method
     * @param string $format
     * @return ResponseInterface
     * @throws GoogleApiException
     */
    protected function getGoogleMapsApiResponse($service, $query, $method = 'GET', $format = 'json')
    {
        if ($service) {
            $service .= '/';
        }
        $uri = $this->googleMapsApiUri . $service . $format;
        $query = array_merge(
            ['key' => $this->placesApiKey],
            $query
        );
        $response = $this->client->request($method, $uri, ['query' => $query]);
        if ($response->getStatusCode() != '200') {
            throw new GoogleApiException('Google Maps API error - status: ' . $response->getStatusCode(), 500);
        }
        return $response;
    }
    /**
     * Query google places API and parse JSON response
     *
     * @param string $service Service identifier in uri. e.g. nearbysearch, details, etc
     * @param array $query Query params
     * @param string $method
     * @return mixed
     * @throws GoogleApiException
     */
    protected function callGoogleMapsApi($service, $query, $method = 'GET')
    {
        $response = $this->getGoogleMapsApiResponse($service, $query, $method);
        $responseData = json_decode($response->getBody(), true);

        switch ($responseData['status']) {
            case 'OVER_QUERY_LIMIT':
                throw new GoogleApiException('Our application reached places query limit', 500);
            case 'REQUEST_DENIED':
                throw new GoogleApiException('We forgot to put a valid key in out app', 500);
            case 'INVALID_REQUEST':
                throw new GoogleApiException('Invalid request', 400);
        }
        unset($responseData['status']);

        return $responseData;
    }

    /**
     * Calculate distance to the Cracow Main Square
     *
     * @param LatLong $location Place to calculate from
     *
     * @return float|int Distance to main square in meters
     */
    protected function calculateMainSquareDistance(LatLong $location)
    {
        $mainSquare = new LatLong(config('core.place.lat'), config('core.place.lng'));
        if ($mainSquare->lat() == $location->lat() && $mainSquare->lng() == $location->lng()) {
            $distance = 0;
        } else {
            $calculator = new Calculator($mainSquare, $location);
            $distance = $calculator->get()->asKilometres() * 1000;
        }
        return $distance;
    }

    /**
     * Remove from the results places that are too far from the main square
     *
     * @param array $results nearby or radar search results - must have geometry.location attribute
     * @return array Filtered results
     */
    protected function filterPlacesFarFromMainSquare($results)
    {
        foreach ($results as $k => $result) {
            $location = $result['geometry']['location'];
            $latlng = new LatLong($location['lat'], $location['lng']);
            $distance = config('core.radius');
            if ($this->calculateMainSquareDistance($latlng) > $distance) {
                unset ($results[$k]);
            }
        }
        // return array values to reset indexes
        return array_values($results);
    }
}