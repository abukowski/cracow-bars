<?php

namespace App\Http\Controllers\Api\v1;

use App\Exceptions\PlacesApiException;
use App\Http\Controllers\Controller;
use App\Places;
use Ballen\Distical\Entities\LatLong;
use Ballen\Distical\Exceptions\InvalidLatitudeFormatException;
use Ballen\Distical\Exceptions\InvalidLongitudeFormatException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PlacesApiController extends Controller
{

    /**
     * @var Places
     */
    protected $places;

    public function __construct(Places $places)
    {
        $this->places = $places;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $locationStr = $request->get('location',
            config('core.place.lat') . ',' . config('core.place.lng')); // main place
        $location = $this->parseLatLng($locationStr);
        $radius = $request->get('radius', config('core.radius'));
        $keyword = $request->get('keyword');
        $other = $keyword ? ['keyword' => $keyword] : [];
        $response = $this->places->getBarsAroundMainSquare($location, $radius, $other);
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = $this->places->getCachedPlaceDetails($id);
        $response = response()->json($details);
        $response->setMaxAge(180);
        $response->setPublic();
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Parse location string
     * @param string $locationStr
     * @return LatLong
     * @throws PlacesApiException
     */
    private function parseLatLng($locationStr)
    {
        try {
            return new LatLong(...explode(',', $locationStr));
        } catch (InvalidLongitudeFormatException | InvalidLatitudeFormatException $exception) {
            throw new PlacesApiException($exception->getMessage(), 400);
        }
    }
}
