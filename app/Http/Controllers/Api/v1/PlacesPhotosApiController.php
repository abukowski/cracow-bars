<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Places;
use Illuminate\Http\Request;

class PlacesPhotosApiController extends Controller
{

    /**
     * @var Places
     */
    protected $places;

    public function __construct(Places $places)
    {
        $this->places = $places;
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $placeId
     * @return \Illuminate\Http\Response
     */
    public function index($placeId)
    {
        $placeDetails = $this->places->getCachedPlaceDetails($placeId);
        $placeData = $placeDetails['result'] ?? [];
        $photos = $placeData['photos'] ?? [];
        foreach ($photos as $k => $photo) {
            $photos[$k]['url'] = route('googlephoto', ['photoId' => $photo['photo_reference']]);
        }
        $response = response()->json($photos);
        $response->setMaxAge(180);
        $response->setPublic();
        return $response;
    }

}
