<?php

namespace App\Http\Controllers;

use App\Places;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class GetGooglePhotoController extends Controller
{

    /**
     * @var Places
     */
    protected $places;

    public function __construct(Places $places)
    {
        $this->places = $places;
    }

    /**
     * Display the specified resource.
     *
     * @param string $photoId
     * @return \Illuminate\Http\Response
     */
    public function getPhoto($photoId)
    {
        $cacheKey = 'photo_' . $photoId;
        [$headers, $responseBody] = Cache::remember($cacheKey, 60*24, function() use($photoId) {
            $response = $this->places->getPhoto($photoId);
            return [$response->getHeaders(), strval($response->getBody())];
        });
        $response = response($responseBody, 200, $headers);
        $response->setPublic();
        $response->setMaxAge(24*60*60);
        $response->setSharedMaxAge(24*60*60);
        return $response;
    }

}
