<?php

namespace App\Http\Middleware;

use App\Exceptions\JsonResponseException;
use Closure;
use Illuminate\Http\Request;

class OnlyJson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @throws JsonResponseException
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->acceptsJson()) {
            throw new JsonResponseException('This API returns only JSON responses', 400);
        }
        return $next($request);
    }
}
