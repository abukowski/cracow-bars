<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Arr;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PlacesTest extends TestCase
{

    //region Reusable methods

    /**
     * @param string $uri
     * @return TestResponse
     */
    protected function getCached($uri)
    {
        static $cache = [];
        if (!array_key_exists($uri, $cache)) {
            $cache[$uri] = $this->get($uri);
        }
        return $cache[$uri];
    }

    /**
     * @return string
     */
    protected function getFirstValidPlaceId()
    {
        $data = $this->getCached('api/v1/places')->decodeResponseJson();
        $firstPlace = Arr::first($data['results']);
        return $firstPlace['place_id'];
    }
    /**
     * @return TestResponse
     */
    protected function getFirstDetailsResponse()
    {
        $placeId = $this->getFirstValidPlaceId();
        $response = $this->getCached('api/v1/places/' . $placeId);
        return $response;
    }

    /**
     * Get places with at least one photo - manually picked one
     * @return string
     */
    protected function getPlaceIdWithPhotos()
    {
        return 'ChIJ9Yu2CqtbFkcROeN37Q9xDuw';
    }

    protected function getPhotosResponse()
    {
        $placeId = $this->getPlaceIdWithPhotos();
        $response = $this->getCached("api/v1/places/$placeId/photos");
        return $response;
    }

    protected function getSomePhotoResponse()
    {
        $placeId = $this->getPlaceIdWithPhotos();
        $responseData = $this->getCached("api/v1/places/$placeId/photos")->decodeResponseJson();
        $firstPhoto = Arr::first($responseData);
        $response = $this->getCached("api/v1/places/$placeId/photos/$firstPhoto[photo_reference]");
        return $response;
    }

    //endregion

    //region places list tests

    public function testPlacesRequestGivesProperResponse()
    {
        $response = $this->getCached('api/v1/places');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    public function testPlacesRequestGivesNonEmptyResult()
    {
        $data = $this->getCached('api/v1/places')->decodeResponseJson();
        $this->assertArrayHasKey('results', $data);
        $this->assertNotEmpty($data['results']);
    }

    public function testPlacesRequestGivesPlacesIdInEachResult()
    {
        $data = $this->getCached('api/v1/places')->decodeResponseJson();
        $place_id = 'place_id';
        foreach ($data['results'] as $result) {
            $this->assertArrayHasKey($place_id, $result);
            $this->assertNotEmpty($result[$place_id]);
        }
    }

    public function testPlacesRequestFarFromMainSquareGivesEmptyResult()
    {
        $location = '53.121971, 18.000374';
        $radius = 2000;
        $response = $this->call('GET', 'api/v1/places', [
            'location' => $location,
            'radius' => $radius
        ]);
        $data = $response->decodeResponseJson();
        $this->assertEmpty($data['results']);
    }

    public function testPlacesRequestInvalidLatLngGivesBadRequestResponse()
    {
        $location = '153.125811,17.8961245';
        $response = $this->call('GET', 'api/v1/places', [
            'location' => $location,
        ]);
        $response->assertStatus(400);
    }
    //endregion

    //region place details tests

    public function testDetailsRequestGivesProperResponse()
    {
        $response = $this->getFirstDetailsResponse();
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    public function testDetailsRequestGivesHttpCacheHeaders()
    {
        $response = $this->getFirstDetailsResponse();
        $response->assertHeader('Cache-Control', 'max-age=180, public');
    }

    public function testDetailsRequestGivesNonEmptyResult()
    {
        $data = $this->getFirstDetailsResponse()->decodeResponseJson();
        $this->assertArrayHasKey('result', $data);
        $this->assertNotEmpty($data['result']);
    }

    public function testDetailsRequestGivesResultWithProperData()
    {
        $response = $this->getFirstDetailsResponse()->decodeResponseJson();
        $data = $response['result'];

        $requiredFields = [
            'place_id', 'name', 'formatted_address', 'photos', 'rating', 'international_phone_number'
        ];
        foreach ($requiredFields as $requiredField) {
            $this->assertArrayHasKey($requiredField, $data, "Missing required field: $requiredField");
        }
    }

    public function testDetailsRequestWithInvalidIdGivesNotFoundError()
    {
        $response = $this->getCached('api/v1/places/123');
        $response->assertStatus(404);
    }
    //endregion

    //region photos tests

    public function testPhotosRequestGivesProperResponse()
    {
        $response = $this->getPhotosResponse();
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
    }

    public function testPhotosRequestGivesNonEmptyResult()
    {
        $responseData = $this->getPhotosResponse()->decodeResponseJson();
        $this->assertNotEmpty($responseData, 'No Photos in response. Maybe try different place id');
    }

    public function testPhotosRequestGivesProperData()
    {
        $responseData = $this->getPhotosResponse()->decodeResponseJson();
        $firstPhoto = Arr::first($responseData);
        foreach (['photo_reference', 'url'] as $requiredField) {
            $this->assertArrayHasKey($requiredField, $firstPhoto);
            $this->assertNotEmpty($firstPhoto[$requiredField]);
        }
    }

    public function testPhotosRequestWithInvalidIdGivesNotFoundError()
    {
        $response = $this->getCached("api/v1/places/123/photos");
        $response->assertStatus(404);
    }
    //endregion

}
