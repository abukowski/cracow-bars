<?php

namespace Tests\Feature\Api;

use Illuminate\Support\Facades\Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GeneralApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testInvalidRequestReturns404()
    {
        $result = $this->get('api/v1/nonexisting');
        $result->assertStatus(404);
    }

    public function testXmlRequestGivesBadRequestResponse()
    {
        $response = $this->get('api/v1/places', [
            'Accept' => 'application/xml'
        ]);
        $response->assertStatus(400);
    }
}
